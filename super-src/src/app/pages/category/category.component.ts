
/**
 * Category Screen
 * @author    ThemesBuckets <themesbuckets@gmail.com>
 * @copyright Copyright (c) 2020
 * @license   AppsPlaces
 */

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CategoryService } from '../../services/category.service';
import { Category } from '../../models/category.model';
import { CartComponent } from '../cart/cart.component';
import { ModalController,Platform, NavController } from '@ionic/angular';
import { Observable } from 'rxjs';

import { TranslateConfigService } from '../../services/translate-config.service';
import { Location } from "@angular/common";
@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss'],
})
export class CategoryComponent implements OnInit {
  
  categories: Observable<any>;
  grid: Boolean = true;
  oneColumn: Boolean = false;
  list: Boolean = false;
  backButtonSubscription:any;
  constructor(private modalController: ModalController,private categoryService: CategoryService,private router: Router,private translateConfigService: TranslateConfigService,private location: Location,public platform: Platform,public navCtrl: NavController) {
  
    this.translateConfigService.getDefaultLanguage();
  }
  ngAfterViewInit() {
    this.backButtonSubscription =  this.platform.backButton.subscribe(()=>{
      this.location.back();
    })
  }
  ngOnDestroy() {
    this.backButtonSubscription.unsubscribe();
  }
  
  ngOnInit() {
    this.categories = this.categoryService.categoryList();
  }

  // Get list of categories
  // getCategories() {
  //   this.categories = this.categoryService.categoryHome();
  // }

  goToProduct(id:number){    
    this.router.navigateByUrl('/tabs/product-category/'+id);  
   }

  // One column view function
  showOneColumn() {
    this.oneColumn = true;
    this.grid = false
    this.list = false;
  }

  // Grid view function
  showGrid() {
    this.grid = true;
    this.oneColumn = false;
    this.list = false;
  }

  // List view function
  showList() {
    this.list = true;
    this.grid = false;
    this.oneColumn = false;
  }

  // Go to cart page function
  async gotoCartPage() {
    const modal = await this.modalController.create({
      component: CartComponent
    });
    return await modal.present();
  }
  beck_to_previous(){
    this.navCtrl.navigateBack('/tabs/tab1');
  }
}
