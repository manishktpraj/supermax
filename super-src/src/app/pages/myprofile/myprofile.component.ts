import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ToastController,LoadingController,NavController} from '@ionic/angular';
import { StorageService } from '../../services/storage.service';
import { FormGroup, FormBuilder, Validators} from '@angular/forms';

import { TranslateConfigService } from '../../services/translate-config.service';

@Component({
  selector: 'app-myprofile',
  templateUrl: './myprofile.component.html',
  styleUrls: ['./myprofile.component.scss'],
})



export class MyprofileComponent implements OnInit {
	uid:any;
    rows:any;
	first_name:any;
	last_name:any;
	email:any;
	mobile_number:any;
	address:any;
	address_2:any;
	city:any;
	state:any;
	country:any;
	pincode:any;
	country_data:any;
	state_data:any;
	ionicForm:any;
  
  
  constructor(public storageService:StorageService,public http:HttpClient,private toastCtrl: ToastController,public loadingCtrl: LoadingController,public navCtrl: NavController,public formBuilder:FormBuilder,private translateConfigService: TranslateConfigService) { 

	this.translateConfigService.getDefaultLanguage();

    this.storageService.getUserData().then(result=>{
      this.uid = result.id;
	  
	  
	  this.loadingCtrl.create({
        message: 'Loading...',       
      }).then((res) => {
        res.present();
		  
    	    let headers = { 'Content-Type': 'application/x-www-form-urlencoded',
						    'Accept': 'application/json'};

    let postData = `uid=${this.uid}`;

    this.http.post<any>(this.storageService.baseUrl+"api/get_profile", postData, {headers})
      .subscribe(data =>
	  {

		if(data.success==1)
		{
		//this.walletdata=data.data;
		res.dismiss();
		 res.onDidDismiss().then((dis) => {
			this.rows=data.details;
			this.state_data=data.states;
			this.country_data=data.countries;
			this.first_name=data.details.frist_name;
			this.last_name=data.details.last_name;
			this.mobile_number=data.details.mobile_number;
			this.address=data.details.address;
			this.address_2=data.details.address_two;
			this.city=data.details.city;
			this.pincode=data.details.zip;
			this.state=data.details.state;
			this.country=data.details.country;
			this.email=data.details.email;
		 });
			
		
      
		}
		else {
			if(data.msg=='nodata')
			{
				res.dismiss();
				  res.onDidDismiss().then((dis) => {
				this.storageService.presentToast("No data found",3000);
				  });
				
			}
			else 
			{
				res.dismiss();
				  res.onDidDismiss().then((dis) => {
				this.storageService.presentToast("Unauthorised Request",3000);
				  });
			}
			
			
		}
		
		
		
       }, error => {
		   res.dismiss();
        console.log(error);
      });
   
	 });
	});

	
  }
  
  ngOnInit() {}
  
  
  async  update_states()
  {
	  
	  
	  
	  
	  
  }

  async update_details()
  {
	
	 this.loadingCtrl.create({
        message: 'Loading...',       
      }).then((res) => {
        res.present();
		  
    	    let headers = { 'Content-Type': 'application/x-www-form-urlencoded',
						    'Accept': 'application/json'};

			let postData = `uid=${this.uid}&frist_name=${this.first_name}&last_name=${this.last_name}&email=${this.email}&mobile_number=${this.mobile_number}&address_two=${this.address_2}&address=${this.address}&city=${this.city}&state=${this.state}&zip=${this.pincode}&country=${this.country}`;
			
			
			
			
			
			
			
			    this.http.post<any>(this.storageService.baseUrl+"api/get_profile", postData, {headers})
      .subscribe(data =>
	  {

		if(data.success==1)
		{
		//this.walletdata=data.data;
		res.dismiss();
		 res.onDidDismiss().then((dis) => {
			this.storageService.presentToast("Sucessfully Updated",3000);
		 });
      
		}
		else {
			if(data.msg=='nodata')
			{
				res.dismiss();
				  res.onDidDismiss().then((dis) => {
				this.storageService.presentToast("No data found",3000);
				  });
				
			}
			else 
			{
				res.dismiss();
				  res.onDidDismiss().then((dis) => {
				this.storageService.presentToast("Unauthorised Request",3000);
				  });
			}
			
			
		}
		
		
		
       }, error => {
		   res.dismiss();
        console.log(error);
      });
	
	  });
	 
	  
  }

}