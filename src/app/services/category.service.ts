import { Injectable } from '@angular/core';
import { Category } from '../models/category.model';
import { HttpClient } from '@angular/common/http';
import { StorageService } from '../services/storage.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  categories: Category[];

  constructor(public storageService: StorageService, public http: HttpClient) { }

  categoryHome(): Observable<any> {

    let body = `limit=2`;
    let headers = { 'Content-Type': 'application/x-www-form-urlencoded' };
    return this.http.post<any>(this.storageService.baseUrl + 'api/get_category', body, { headers }).pipe(map(categories => categories.category_detail))

  }
  categoryList(id:any): Observable<any> {

    let body = `cat=${id}`;
    let headers = { 'Content-Type': 'application/x-www-form-urlencoded' };
    return this.http.post<any>(this.storageService.baseUrl + 'api/get_category', body, { headers }).pipe(map(categories => categories.category_detail))

  }

}
