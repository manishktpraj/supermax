import { Injectable } from '@angular/core';
import { TranslateConfigService } from '../services/translate-config.service';
import { TranslateService } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { StorageService } from '../services/storage.service';

@Injectable({
  providedIn: 'root'
})
export class PagesService {
  constructor(private translateConfigService: TranslateConfigService,public translateService:TranslateService,public http:HttpClient,private storageService:StorageService) { 
     this.translateConfigService.getDefaultLanguage();
	
  
   }
   
  

  getPages() {

    
	  
    return [
      {
        title: 'menu.home',
        url: '/tabs/tab1',
        icon: 'home',
		link:0
      },
      {
        title: 'address.my_address',
        url: '/myaddresses',
        icon: 'business',
		link:0
      },
      {
        title: 'order.title1',
        url: '/tabs/tab4',
        icon: 'checkmark-circle',
		link:0
      },
      {
        title: 'cart.my_cart',
        url: '/tabs/tab3',
        icon: 'cart',
		link:0
      },
      // {
      //   title: 'Notification',
      //   url: '/tabs/tab4',
      //   icon: 'notifications'
      // },
      {
        title: 'menu.offer_zone',
        url: 'tabs/tab2',
        icon: 'grid',
		link:0
      },
	  {
        title: 'menu.about',
        url: '/about',
        icon: 'information-circle',
		link:0
      },
      {
        title: 'menu.terms_condition',
        url: '/terms-and-condition',
        icon: 'create',
		link:0
      },
	  {
        title: 'menu.privacy_policy',
        url: '/privacy-policy',
        icon: 'eye-off',
		link:0
      },
    ];
  }
}
