import { Component, OnInit } from '@angular/core';
import { Router , ActivatedRoute} from '@angular/router';
import { ModalController, LoadingController,Platform, NavController } from '@ionic/angular';
import { CategoryService } from '../../services/category.service';
import { Category } from '../../models/category.model';
import { HttpClient } from '@angular/common/http';
import { StorageService } from '../../services/storage.service';
import { Observable } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { TranslateConfigService } from '../../services/translate-config.service';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss'],
})
export class CategoriesComponent implements OnInit {
  
  pid:any;
  default_src:'//supermaxmart.in/dist/img/placeholder.png';
  categories: Observable<any>;
  constructor(private categoryService: CategoryService,private activatedRoute: ActivatedRoute,private translate: TranslateService,public loadingCtrl: LoadingController,public storageService: StorageService,public http:HttpClient,private router: Router,private translateConfigService: TranslateConfigService) {

    this.translateConfigService.getDefaultLanguage();

  }

  ngOnInit() {
	
	this.categories = this.categoryService.categoryList(0); 
	
  }
  
  goToProduct(id:number,subcat:any){
	if(subcat)
	{	
		this.storageService.setParentCat(id);
		this.router.navigateByUrl('/tabs/categories');  
	}
	else
	{   
		this.router.navigateByUrl('/tabs/product-category/'+id);  
	}
  }
  
  
  seeAllCat()
  {
	  
	this.storageService.setParentCat(0);  
	this.router.navigateByUrl('/tabs/categories');  
  }
 

  // getCategories() {
  //   this.categories = this.categoryService.categoryList();
  // }

}
