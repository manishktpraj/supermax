import { Component, OnInit } from '@angular/core';
import { ModalController, NavController } from '@ionic/angular';
import { DealsService } from '../../services/deals.service';
import { HttpClient } from '@angular/common/http';
import { Product } from '../../models/product.model';
import { ProductsService } from '../../services/products.service';
import { StorageService } from '../../services/storage.service';


import { Observable } from 'rxjs';
import { ProductDetailsComponent } from '../../pages/product-details/product-details.component';

import { TranslateConfigService } from '../../services/translate-config.service';

@Component({
  selector: 'app-hot-deals',
  templateUrl: './hot-deals.component.html',
  styleUrls: ['./hot-deals.component.scss'],
})
export class HotDealsComponent implements OnInit {

  // Slider Options
  slideOpts = {
    initialSlide: 0,
    speed: 400,
    slidesPerView: 2,
  };
products: Product;
  deals: Observable<any>;
  userId="";

  constructor(private dealsService: DealsService, public productsService: ProductsService,public storageService: StorageService, public http: HttpClient,private modalController: ModalController,private translateConfigService: TranslateConfigService, public navCtrl: NavController) { 
  
    this.storageService.getUserData().then(userData=>{
      //console.log(userData);
      if(userData!=null){
        //this.userId = userData['id'];
        this.deals = this.dealsService.getDealsAll(userData['id']);
      }else{
        this.deals = this.dealsService.getDealsAll("");
      }

     
    });
    this.translateConfigService.getDefaultLanguage();
  }

  ngOnInit() {
    
    
  }

  goToHome(){
    this.navCtrl.navigateBack('/tabs/tab2');
  }
    async goToProductDetails(product) {
   // console.log(product);
    const modal = await this.modalController.create({
      component: ProductDetailsComponent,
      componentProps: product
    });
    modal.onDidDismiss()
    .then((data) => {     

      this.storageService.getUserData().then(userData=>{
        if(userData!=null){
          this.userId = userData['id'];
          this.productsService.featuredProduct(userData['id']).subscribe(result=>{
            this.products = result;
          });
        }else{
           this.productsService.featuredProduct("").subscribe(result=>{
            this.products = result;
          });
        }
      });

    });
    return await modal.present();
  }


}
