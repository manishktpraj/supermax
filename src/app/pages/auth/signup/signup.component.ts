import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ToastController,LoadingController,NavController,ModalController} from '@ionic/angular';
import { StorageService } from '../../../services/storage.service';
import { SigninComponent } from '../signin/signin.component';
import { TranslateConfigService } from '../../../services/translate-config.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss'],
})
export class SignupComponent implements OnInit {
  first_name:any;
  last_name:any;
  otp:any;
  email:any;
  mobile_number:any;
  step=1;
  resendotp=false;
  password:any;
  constructor(public http:HttpClient,private toastCtrl: ToastController,public loadingCtrl: LoadingController,public storageService: StorageService,public navCtrl: NavController,private translateConfigService: TranslateConfigService,private modalController:ModalController,private translate:TranslateService) { 
    this.translateConfigService.getDefaultLanguage();
	this.storageService.getMobile().then(data => {
		if(data)
		{
			this.mobile_number=data;
		}
	});
	
  }

  ngOnInit() {}

	goto_next_step()
	{
		if(this.step==1)
		{
			if(this.mobile_number>1000000000 && this.mobile_number<999999999999)
			{
				
				this.sendotp();
				
				
			}
			else
			{
				this.storageService.presentToast(this.translate.instant("account.enter_phone"),2000);
			}
			
		}
		else if(this.step==2)
		{
			if(this.otp>100000 && this.otp<999999)
			{
				this.verifyotp();
			}
			
			else
			{
				this.storageService.presentToast(this.translate.instant("account.enter_otp"),2000);
				
			}
			
			
		}
		else if(this.step==3)
		{
			
			
	if(this.first_name=="" || this.first_name==undefined){     
      this.storageService.presentToast(this.translate.instant("account.first_name"),2000);
    }else if(this.last_name=="" || this.last_name==undefined){    
      this.storageService.presentToast(this.translate.instant("account.last_name"),2000);
    }else if(this.password=="" || this.password==undefined){      
      this.storageService.presentToast(this.translate.instant("account.enter_password"),2000);
    }else{
      this.loadingCtrl.create({
        message: this.translate.instant("account.loading"),       
      }).then((res) => {
        res.present();
        let body = `first_name=${this.first_name}&last_name=${this.last_name}&email=${this.email}&mobile_number=${this.mobile_number}&password=${this.password}`;

          let headers = { 'Content-Type': 'application/x-www-form-urlencoded' };
          this.http.post<any>(this.storageService.baseUrl+'api/user_register', body, {headers} ).subscribe(data => {
             // console.log(data);
              res.dismiss();
              res.onDidDismiss().then((dis) => {
				  if(data.status==true)
				  {
               // this.navCtrl.navigateForward('/landing');
              const toast = this.toastCtrl.create({
                message: this.translate.instant("account.reg_success"),
                duration: 2000
              }).then((toastData)=>{
                this.modalController.dismiss();
				this.storageService.setUserData(data.user_detail);
                toastData.present();
              });
				  }
				  else 
				  {
					  
					const toast2 = this.toastCtrl.create({
                message: data.msg,
                duration: 2000
              }).then((toastData)=>{
                //this.modalController.dismiss();
                toastData.present();
              }); 
				  }
             });
        })
   
       
      });
    }
			
			
		}
		else{
			//
		}
		
		
		
	}
  
		sendotp()
		{
		
	   this.loadingCtrl.create({
        message: this.translate.instant("account.loading"),
      }).then((res) => {
		  
		 if(this.mobile_number > 1000000000 || this.mobile_number < 999999999999)
			 
			 {
				 
        res.present();
        let body = `mobile=${this.mobile_number}`;
        let headers = { 'Content-Type': 'application/x-www-form-urlencoded' };
        this.http.post<any>(this.storageService.baseUrl + 'api/send_otp_tmp', body, { headers }).subscribe(data => {
          if (data.status == true) {
			res.dismiss();
            res.onDidDismiss().then((dis) => {
			///	this.storageService.presentToast(this.translate.instant("account.otp_sent"), 3000);
        ///this.verifyotp();
 
		 	this.step=2;
            });
            
          } else {
            res.dismiss();
            res.onDidDismiss().then((dis) => {
				if(data.signin==true)
				{
					this.storageService.presentToast(data.msg, 3000);
					this.modalController.dismiss();
					this.openSignIn();
					
					
				}
				else
				{
					this.storageService.presentToast(data.msg, 3000);
				}
              

          });
		
		  }

        });
			 }
			 
			 else
			 {
				res.dismiss();
				res.onDidDismiss().then((dis) => {
				this.storageService.presentToast(this.translate.instant("login.invalid_mobile"), 3000);

				 
			 });

      }
			
			
			
			

		});
		
		}
		
		
				verifyotp()
		{
		
	   this.loadingCtrl.create({
        message: this.translate.instant("account.loading"),
      }).then((res) => {
		 				 
        res.present();
     let body = `mobile=${this.mobile_number}&otp=${this.otp}`;
    ////    let body = `mobile=${this.mobile_number}&otp=123456`;
        let headers = { 'Content-Type': 'application/x-www-form-urlencoded' };
        this.http.post<any>(this.storageService.baseUrl + 'api/verify_otp_tmp', body, { headers }).subscribe(data => {
          if (data.status == true) {  
            res.dismiss();
            res.onDidDismiss().then((dis) => {
		////		this.storageService.presentToast(this.translate.instant("account.otp_verified"), 3000);
				this.step=3;
            });
            
          } else {
            res.dismiss();
            res.onDidDismiss().then((dis) => {
              this.storageService.presentToast(data.msg, 3000);

          });
		
		  }

        });

		});
		
		}
		
  closeSignup(){
    this.modalController.dismiss();
  }
  
  


 async openSignIn(){
    const modal = await this.modalController.create({
      component: SigninComponent      
    });
    return await modal.present();
  }
  
 
}
