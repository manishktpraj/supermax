/**
 * Product Details Screen
 * @author    ThemesBuckets <themesbuckets@gmail.com>
 * @copyright Copyright (c) 2020
 * @license   AppsPlaces
 */

import { Component, OnInit, Input } from '@angular/core';
import { Product } from '../../models/product.model';
import { StorageService } from '../../services/storage.service';
import { CartComponent } from '../cart/cart.component';
import { ModalController, AlertController, LoadingController ,Platform} from '@ionic/angular';
import { ProductsService } from '../../services/products.service';
import { TranslateConfigService } from '../../services/translate-config.service';
import { HttpClient } from '@angular/common/http';
import { SigninComponent } from '../../pages/auth/signin/signin.component';
import { TranslateService } from '@ngx-translate/core';
import { AddRatingPage } from '../add-rating/add-rating.page';
@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.scss'],
})

export class ProductDetailsComponent implements OnInit {

  @Input() id: number;
  @Input() name: String;
  @Input() description: String;
  @Input() original_price: number;
  @Input() discount_price: number;
  @Input() default_image: String;
  @Input() product_size: Array<String>;
  @Input() product_color: Array<String>;
  @Input() product_reviews: any;
  @Input() currency_symble: any;
  @Input() isWishlist: number;
  @Input() stock_unit: any;
  @Input() order_limit: any;


  products: Product;

  // Slider Options
  slideOpts = {
    initialSlide: 0,
    loop: true,
    autoplay: false,
    speed: 400,
    pagination: {
      el: '.swiper-pagination',
      dynamicBullets: true,
    },
  };

  customSize: any;
  customColor: any;
  selected_color: any;
  selected_size: any;
  color:any;
  size :any;
  quantity:number;
  color_name:any;
  flag=0;
  default_src:'//supermaxmart.in/dist/img/placeholder.png'

  constructor(public modalController: ModalController,
    public storageService: StorageService, public alertController: AlertController, public productsService: ProductsService, private translateConfigService: TranslateConfigService, public http: HttpClient, public loadingCtrl: LoadingController, private platform: Platform,private translate:TranslateService) {
    this.translateConfigService.getDefaultLanguage();
	this.quantity=1;
  }

  ionViewWillEnter(){
  
     // console.log(this.product_color);
      this.customColor = this.product_color[0]['color_id'];
      this.product_size = this.product_color[0]['product_size'];
	  this.original_price = this.product_color[0]['original_price'];
	  this.discount_price = this.product_color[0]['discount_price'];
	  this.default_image = this.storageService.baseUrl+"uploads/medium/"+this.product_color[0]['photo'];
      this.color = this.product_color[0]['id']; // default selected
    // console.log(this.product_size);
      if(this.product_size.length!=0){
        this.size = this.product_color[0]['product_size'][0]['size_id'];
      }
      this.selected_color = this.product_color[0]['color_id'];
	  this.color_name = this.product_color[0]['color_name'];
  }
  ngOnInit() {
   

  }
  
  sizeChange(item) {
   // console.log(item.target.value);
    for(let i=0;i<this.product_color.length;i++){
      if(this.product_color[i]['id']==item.target.value){
		  this.color_name = this.product_color[i]['color_name'];
		  this.original_price = this.product_color[i]['original_price'];
			this.discount_price = this.product_color[i]['discount_price'];
		this.default_image = this.storageService.baseUrl+"uploads/medium/"+this.product_color[i]['photo'];
        if(this.product_color[i]['product_size'].length!=0){
          this.product_size = this.product_color[i]['product_size'];
          this.size=this.product_size[0]['size_id'];
		  
		  
        } 
		this.stock_unit		= this.product_color[i]['stock_unit'];
		this.selected_color = this.product_color[i]['color_id']; 		
      }
    }   
  }
  selectColor(item) {
    this.customColor = item['id'];
    this.selected_size = item.target.value;
  }


	plusQuantity()
	{
		if(this.quantity<this.order_limit)
		{
			this.quantity=this.quantity+1;
		}
		else 
		{
			this.storageService.presentToast(this.translate.instant("product.order_limit"), 2000);
		}
		
	}
	
	minusQuantity()
	{
		if(this.quantity>1)
		{
			this.quantity=this.quantity-1;
		}
	}
  // Add to Cart Function
  async addToCart() {

    if (this.selected_color == undefined || this.selected_color == null) {
      	this.storageService.presentToast(this.translate.instant("product.order_limit"), 2000);
    } else {
		      this.storageService.getStorage('my-cart').then((products) => {
				  if(products){
				for (var i = 0; i < products.length; i++) {
					if (products[i].id == this.id) {
							//console.log(Number(products[i].quantity)+Number(this.quantity));
							//console.log(Number(this.stock_unit));
							if(Number(products[i].quantity)+Number(this.quantity)>Number(this.order_limit))
							{
								this.flag=1;
							}
							
						}
				}
			}
			
			if(this.flag==1)
		{
			this.storageService.presentToast(this.translate.instant("product.order_limit"), 2000);
		}
		else
		{
			  this.products = {
				id: this.color, // this is actually variation id 
				product_id:this.id,
				name: this.name,
				description: this.description,
				original_price: this.original_price,
				discount_price: this.discount_price,
				default_image: this.default_image,
				product_size: this.customSize,
				product_color:this.customColor,
				quantity: this.quantity,
				product_reviews: this.product_reviews,
				stock_unit: this.stock_unit,
				order_limit: this.order_limit,
				isWishlist: this.isWishlist,
				currency_symble: this.currency_symble,
				selected_color: this.selected_color,
				selected_size: this.color_name
			  }

		// Save cart product in storage
		this.storageService.setStorageValue(this.products, 'my-cart');
		}	
			});
		
		
		
      /*const alert = await this.alertController.create({
        header: this.translate.instant("cart.confirm"),
        message: this.translate.instant("cart.add_to_cart"),
        buttons: [
          {
            text: this.translate.instant("order.cancel"),
            role: 'cancel',
            cssClass: 'secondary',
            handler: (blah) => {
              console.log('Confirm Cancel: blah');
            }
          }, {
            text: this.translate.instant("cart.okay"),
            handler: () => {

              this.storageService.setStorageValue(this.products, 'my-cart');

            }
          }
        ]
      });

      await alert.present();
	  */

    }


  }


  changeWishlistStatus(status) {

    this.storageService.getStorage('userDetails').then((userData) => {
      if (userData != null) {
        this.updateWishlist(userData['id'], status);
      } else {
        this.signIn();
      }
    });

  }

  async signIn(){
    const modal = await this.modalController.create({
      component: SigninComponent
    });

        modal.onDidDismiss()
      .then((data) => {
      
      });
     await modal.present();
  }

  async updateWishlist(userId, status) {


    const alert = await this.alertController.create({
      header: this.translate.instant("cart.confirm"),
      message:  this.translate.instant("product_details.add_to_wishlilst"),
      buttons: [
        {
          text: this.translate.instant("order.cancel"),
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: this.translate.instant("cart.okay"),
          handler: () => {
            this.loadingCtrl.create({
              message: this.translate.instant("account.loading"),
            }).then((res) => {
              res.present();
              let body = `product_id=${this.id}&status=${status}&user_id=${userId}&color_id=${this.customColor}&size_id=`;
              let headers = { 'Content-Type': 'application/x-www-form-urlencoded' };
              return this.http.post<any>(this.storageService.baseUrl + 'api/add_wishlist', body, { headers }).subscribe((result) => {
                if (result.status == true) {
                  this.isWishlist = 1;
                }else{
                  this.isWishlist = 0;
                }
                res.dismiss();
              })
            });


          }
        }
      ]
    });

    await alert.present();

  }

  // Go to cart page
  async gotoCartPage() {
    this.dismiss();
    const modal = await this.modalController.create({
      component: CartComponent
    });
    return await modal.present();
  }

  // Back to previous page function
  dismiss() {
    this.modalController.dismiss({
      'dismissed': true
    })
  }

 
  openAddReview(){
    this.storageService.getStorage('userDetails').then((userData) => {
      if (userData != null) {
        this.openReviewModal();
      } else {
        this.signIn();
      }
    });
  }


  async openReviewModal(){
    const modal = await this.modalController.create({
      component: AddRatingPage,
      componentProps: {pro_id:this.id}
    });
    modal.onDidDismiss()
    .then((data) => {     
          
      this.product_reviews =data.data.product_review;
    });
    return await modal.present();
  }
  
 updateUrl($event) {
    this.default_image = this.default_src;
  }
}
