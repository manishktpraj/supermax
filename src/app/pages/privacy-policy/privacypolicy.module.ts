import { NgModule , CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { PrivacypolicyComponent } from './privacypolicy.component';
import { TranslateModule } from '@ngx-translate/core';
@NgModule({
  imports: [
    CommonModule,
	ReactiveFormsModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild([
      {
        path: '',
        component: PrivacypolicyComponent
      }
    ]),
    TranslateModule,
	NgxDatatableModule,
  ],
  declarations: [PrivacypolicyComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PrivacypolicyModule { }
