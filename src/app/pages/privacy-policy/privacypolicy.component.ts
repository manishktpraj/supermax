import { Component, OnInit } from '@angular/core';
import { ViewEncapsulation } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ToastController,LoadingController,NavController} from '@ionic/angular';
import { StorageService } from '../../services/storage.service';
import { TranslateConfigService } from '../../services/translate-config.service';


@Component({
  selector: 'app-privacypolicy',
  templateUrl: './privacypolicy.component.html',
  styleUrls: ['./privacypolicy.component.scss'],
   encapsulation: ViewEncapsulation.None,

})



export class PrivacypolicyComponent implements OnInit {
 privacytxt:any;
  
  constructor(public storageService:StorageService,public http:HttpClient,private toastCtrl: ToastController,public loadingCtrl: LoadingController,public navCtrl: NavController,private translateConfigService: TranslateConfigService) { 

	this.translateConfigService.getDefaultLanguage();

  
 this.loadingCtrl.create({
        message: 'Loading...',       
      }).then((res) => {
        res.present();
	
	    let headers = { 'Content-Type': 'application/x-www-form-urlencoded',
						'Accept': 'application/json'};

    let postData = ``;

    this.http.post<any>(this.storageService.baseUrl+"api/get_privacy_text", postData, {headers})
      .subscribe(data =>
	  {

		if(data.success==1)
		{
			//console.log(data);
		//this.walletdata=data.data;
		res.dismiss();
                res.onDidDismiss().then((dis) => {
					this.privacytxt=data.text;
				});
      
		}
		else {
			//console.log(data);
			
		res.dismiss();
                res.onDidDismiss().then((dis) => {
					this.privacytxt="No data found";
				});	
			
		}
		
		
		
       }, error => {

		   res.dismiss();
                res.onDidDismiss().then((dis) => {
							  
					this.privacytxt="No data found";
				});	
			
      });
    });

  
	
	
  }
	
  
  
  ngOnInit() {}
  



}